{
    "id": "c21fc6cc-7025-4c68-ae5a-a33822a5f929",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ball",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f29094e6-80d5-472b-b94b-abd1cee71676",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c21fc6cc-7025-4c68-ae5a-a33822a5f929",
            "compositeImage": {
                "id": "34313c34-eb3d-4876-9785-f4c5a67a7704",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f29094e6-80d5-472b-b94b-abd1cee71676",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9f0e25f-154c-4d98-9871-64a7328316a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f29094e6-80d5-472b-b94b-abd1cee71676",
                    "LayerId": "ecb0c51a-04cb-4527-864c-e784261fd267"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ecb0c51a-04cb-4527-864c-e784261fd267",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c21fc6cc-7025-4c68-ae5a-a33822a5f929",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}