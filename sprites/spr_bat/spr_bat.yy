{
    "id": "8ac9239c-2349-4267-ac6e-96b1edd77f92",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bat",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "957bbce1-58e2-4a6c-b32e-e51e3bfc524d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ac9239c-2349-4267-ac6e-96b1edd77f92",
            "compositeImage": {
                "id": "1ee1d425-6064-4c64-a3ad-759f0dce7b56",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "957bbce1-58e2-4a6c-b32e-e51e3bfc524d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a350d28-0b61-4baf-88f2-0be985fc9873",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "957bbce1-58e2-4a6c-b32e-e51e3bfc524d",
                    "LayerId": "79d5ebc2-b62b-4ff8-bf87-69b29663a838"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "79d5ebc2-b62b-4ff8-bf87-69b29663a838",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ac9239c-2349-4267-ac6e-96b1edd77f92",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 8
}