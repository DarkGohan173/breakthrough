{
    "id": "207d9591-0805-4ac3-b85f-a097b30ace8e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_brick",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ace400c2-ac79-4799-a674-ab429957e8f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "207d9591-0805-4ac3-b85f-a097b30ace8e",
            "compositeImage": {
                "id": "2f82962a-babe-49b1-8b77-f6d549a44943",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ace400c2-ac79-4799-a674-ab429957e8f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0040947-b315-4098-ab20-104f657c0e57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ace400c2-ac79-4799-a674-ab429957e8f8",
                    "LayerId": "c9b2dbb7-cb3c-4d65-8b4f-d2c66917b369"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c9b2dbb7-cb3c-4d65-8b4f-d2c66917b369",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "207d9591-0805-4ac3-b85f-a097b30ace8e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 8
}