{
    "id": "e5173788-65a1-4121-883c-d80f86c62096",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bat",
    "eventList": [
        {
            "id": "fcb5c32f-94d5-4ab7-abea-58aed3e1def1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 37,
            "eventtype": 5,
            "m_owner": "e5173788-65a1-4121-883c-d80f86c62096"
        },
        {
            "id": "7cf00f2e-5d9c-4ceb-89d7-ea838f394489",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 39,
            "eventtype": 5,
            "m_owner": "e5173788-65a1-4121-883c-d80f86c62096"
        },
        {
            "id": "273deefd-5e00-42ac-be7d-138c0bfcfa65",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e5173788-65a1-4121-883c-d80f86c62096"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8ac9239c-2349-4267-ac6e-96b1edd77f92",
    "visible": true
}