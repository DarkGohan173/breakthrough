{
    "id": "42da0595-6b0e-4797-ab1a-fad5c62a2043",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ball",
    "eventList": [
        {
            "id": "b2530c0a-d658-4a8f-9f33-6ccab7261853",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "42da0595-6b0e-4797-ab1a-fad5c62a2043"
        },
        {
            "id": "f28e2e27-e617-48cd-984b-ee0a2383cf3f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "42da0595-6b0e-4797-ab1a-fad5c62a2043"
        },
        {
            "id": "874cac22-2514-46ad-8115-2c0389e1d1fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "42da0595-6b0e-4797-ab1a-fad5c62a2043"
        },
        {
            "id": "4da9621a-bdef-4a5f-a866-268e6d4efa21",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "c971393b-9969-4341-9720-71cc2316299f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "42da0595-6b0e-4797-ab1a-fad5c62a2043"
        },
        {
            "id": "344c9d0d-00b4-43d1-b59b-899525468009",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "e5173788-65a1-4121-883c-d80f86c62096",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "42da0595-6b0e-4797-ab1a-fad5c62a2043"
        },
        {
            "id": "14ce1166-0f3a-4769-9b94-46140063ad2d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 7,
            "m_owner": "42da0595-6b0e-4797-ab1a-fad5c62a2043"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c21fc6cc-7025-4c68-ae5a-a33822a5f929",
    "visible": true
}